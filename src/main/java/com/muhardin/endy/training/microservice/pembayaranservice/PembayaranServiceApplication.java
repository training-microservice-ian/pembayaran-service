package com.muhardin.endy.training.microservice.pembayaranservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PembayaranServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PembayaranServiceApplication.class, args);
	}
}
