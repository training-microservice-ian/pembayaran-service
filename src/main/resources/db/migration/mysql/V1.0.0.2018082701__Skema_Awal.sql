create table produk (
    id varchar (36),
    kode varchar(20) not null,
    nama varchar(100) not null,
    primary key (id), 
    unique (kode)
) Engine=InnoDB;

create table pembayaran (
    id varchar(36),
    id_produk varchar(36) not null,
    nasabah varchar(36) not null,
    nilai numeric (19,2) not null,
    waktu_transaksi datetime not null,
    primary key (id)
) Engine=InnoDB;